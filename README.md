<h1 align="center">
  UI - VASCO TV+
</h1>

<p align="center">
  <img src=".github/preview.png" width="100%" />
</p>

## 💻 Project

This project is a study project where I create the UI of a site simulating an exclusive platform for the YouTube channel "VASCO TV" using HTML and CSS.

## 🚀 Technology

- HTML
- CSS

## 📔 Knowledge covered

- [x] Semantic use of HTML
- [x] Use of global variables in `:root`
- [x] Interactive menu
- [x] Improved image loading performance
- [x] Add favicon
- [x] Event to add a video
- [x] Application responsiveness with `@media`

## 💻 Browse the site

https://vascotvplus.netlify.app
